To install package require it through composer.

1. Register ```OpcacheHelper\OpcacheHelperServiceProvider```
2. Publish config ```php artisan vendor:publish --provider="OpcacheHelper\OpcacheHelperServiceProvider" --tag=config```
3. Set ```OPCACHE_HELPER_AUTH_KEY``` in your env


Usage:
1. ```php artisan opcache-helper:drop-cache``` to drop fpm opcache
2. POST /opcache-helper/opcache/drop to drop manually via routes
3. POST /opcache-helper/opcache/status to get opcache status