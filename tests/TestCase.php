<?php
declare(strict_types=1);

namespace OpcacheHelperTests;

use Illuminate\Support\Facades\Route;
use OpcacheHelper\OpcacheHelperServiceProvider;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    protected $testAuthKey = 'asdasdasd';

    /**
     * Setup the test environment.
     */
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * add the package provider
     *
     * @param $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            OpcacheHelperServiceProvider::class
        ];
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('opcache-helper.auth_key', $this->testAuthKey);
    }
}