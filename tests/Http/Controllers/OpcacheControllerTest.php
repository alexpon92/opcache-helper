<?php
declare(strict_types=1);

namespace OpcacheHelperTests\Http\Controllers;

use OpcacheHelperTests\TestCase;

class OpcacheControllerTest extends TestCase
{
    public function testAuth(): void
    {
        $response = $this->post(
            route('opcache_helper.opcache.status'),
            [],
            [
                'opcache-auth-key' => $this->testAuthKey
            ]
        );
        $response->assertStatus(200);
    }

    public function testAuthFailed(): void
    {
        $response = $this->post(
            route('opcache_helper.opcache.status'),
            [],
            [
                'opcache-auth-key' => '123412312'
            ]
        );
        $response->assertStatus(403);
    }
}