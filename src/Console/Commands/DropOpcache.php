<?php
declare(strict_types=1);

namespace OpcacheHelper\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;

class DropOpcache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'opcache-helper:drop-cache {--base_uri=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean opcache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(Client $httpClient)
    {
        $baseUri = $this->option('base_uri') ?? config('opcache-helper.base_uri');
        $config = $httpClient->getConfig();
        $config['base_uri'] = $baseUri;

        $httpClient = new Client($config);
        $httpClient->request(
            'POST',
            route('opcache_helper.opcache.drop', [], false),
            [
                'headers' => [
                    'opcache-auth-key' => config('opcache-helper.auth_key')
                ]
            ]
        );
    }
}