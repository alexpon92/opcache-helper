<?php
declare(strict_types=1);

namespace OpcacheHelper\Http\Middleware;

use Illuminate\Http\Request;
use Closure;

class AuthorizeRequest
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $authHeader = $request->header('opcache-auth-key');
        if ($authHeader !== config('opcache-helper.auth_key')) {
            return response('Bad auth key', 403);
        }

        return $next($request);
    }
}