<?php
declare(strict_types=1);

namespace OpcacheHelper\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class OpcacheController extends BaseController
{
    public function status()
    {
        return response(
            json_encode(
                [
                    'host'        => gethostname(),
                    'cache_state' => opcache_get_status(config('opcache-helper.opcache.status.with_files'))
                ]
            ),
            200,
            [
                'Content-type' => 'application/json'
            ]
        );
    }

    public function drop()
    {
        opcache_reset();
        return response('', 200);
    }
}