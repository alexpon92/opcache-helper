<?php
declare(strict_types=1);

namespace OpcacheHelper;

use Illuminate\Support\ServiceProvider;
use OpcacheHelper\Console\Commands\DropOpcache;

class OpcacheHelperServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/routes.php');
    }

    public function register()
    {
        if ($this->app->runningInConsole()) {
            $this->commands(
                [
                    DropOpcache::class,
                ]
            );
        }

        $this->publishes(
            [
                __DIR__ . '/config/opcache-helper.php' => config_path('opcache-helper.php'),
            ],
            'config'
        );

        $this->mergeConfigFrom(
            __DIR__ . '/config/opcache-helper.php',
            'opcache-helper'
        );
    }
}