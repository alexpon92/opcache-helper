<?php

return [
    'auth_key' => env('OPCACHE_HELPER_AUTH_KEY'),

    'opcache' => [
        'status' => [
            'with_files' => true,
        ]
    ],

    'base_uri' => env('APP_URL')
];
