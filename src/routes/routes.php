<?php

Route::prefix('opcache-helper')->middleware([\OpcacheHelper\Http\Middleware\AuthorizeRequest::class])->group(
    static function () {
        Route::prefix('opcache')->group(
            static function () {
                Route::post('status', 'OpcacheHelper\Http\Controllers\OpcacheController@status')
                     ->name('opcache_helper.opcache.status');

                Route::post('drop', 'OpcacheHelper\Http\Controllers\OpcacheController@drop')
                     ->name('opcache_helper.opcache.drop');
            }
        );
    }
);